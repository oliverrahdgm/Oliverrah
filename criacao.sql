create table funcionario(
	id serial,
	matricula varchar(6) unique not null,
	nome varchar(100) not null,
	primary key (id)
);

create table carro (
	id serial,
	modelo varchar(20) not null,
	marca varchar(20),
	placa char(8) unique not null,
	primary key(id)
);

create table carroFuncionario(
	idFuncionario int not null,
	idCarro int not null,
	foreign key (idFuncionario) references funcionario(id),
	foreign key (idCarro) references carro(id)
);

create table endereco(
	id serial,
	cep char(8),
	numero varchar(20) not null,
	bairro varchar(30) not null,
	lagradouro varchar(80) not null,	
	primary key(id)
);

create table cliente(
	id serial,
	cpf char(11) unique not null,
	nome varchar(100) not null,
	dataNascimento date not null,
	sexo char(1) check(sexo in('m','f','o')),
	cep char(8) not null,
	idEndereco int not null,
	primary key(id),
	foreign key(idEndereco) references endereco(id)
);

create table alugaLocacao(
	dataLocacao date not null,
	dataDevolucao date not null,
	valorTotal decimal(6,2) not null,
	idCliente int not null,
	idCarro int not null,
	foreign key(idCliente) references cliente(id),
	foreign key(idCarro) references carro(id)
);

create table telefone(
	id serial,
	numero varchar(14) not null,
	idCliente int not null,
	primary key(id),
	foreign key(idCliente) references cliente(id)
);
				