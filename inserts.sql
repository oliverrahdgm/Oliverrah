insert into endereco(lagradouro,numero,bairro) values 
('Rua Dom João V','120','Miramar'),
('Rua Padre Ibiapina','56','Cabo Branco'),
('Rua Boa Esperança','89','Miramar'),
('Rua Dom João V','310','Miramar'),
('Rua da Paz','21','Bancários');

insert into cliente(cpf,nome,sexo,dataNascimento,idEndereco) values 
('11111122222','Maria Fernanda Nobrega','f','1990-04-12',1),
('96452997623','Natalia Campos','o','1998-07-02',2),
('75432334512','Jorge Alencar','m','1993-01-23',3),
('9746356654','Gabriel Nascimento','o','1998-04-12',4),
('74635928374','Leticia Alencar','f','1988-09-01',5);

insert into carro(modelo,marca,placa) values 
('spin','chevrolet','ABC-1342'),
('siena','fiat','PPG-7763'),
('soul','kia','AAA-5522'),
('sandero','renault','NAU-8332');

insert into funcionario(matricula,nome) values
('314264','Paulo Andrade'),
('089785','Vitoria Veloso');

insert into alugaLocacao(dataLocacao,dataDevolucao,idCliente,idCarro,valorTotal) values
('2018-08-24','2018-08-25','8','5','100.00'),
('2018-08-25','2018-08-26','9','6','100.00'),
('2018-08-26','2018-08-27','10','7','80.00');

